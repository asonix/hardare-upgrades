# Cluster & Storage Upgrade Project

## Goals:
1. Fast on-node storage for k8s+ceph, useful of containers requiring a little bit of block storage
  - Nextcloud's configuration files + php scripts
  - pict-rs's sled DB
  - relay's sled DB
  - conduit's rocksdb
  - redis' storage
  - possibly gitea's git repos
2. Off-cluster object-storage (probably with Garage, existing hardware - nextcloud box)
  - Nextcloud's data
  - pict-rs's data
  - synapse's data
3. Retire GlusterFS Pool
  - Frees up 3 rock64s for application nodes (or otherwise)

Stretch:
1. Move Postgres onto SoQuartz blades (cost: $363 or $726)
  - Frees up 4 rock64s for application nodes (or otherwise)
2. Retire Raspberry Pi 4s
  - Give to friends or something

## Devices:

| Count | Cost (per) | Cost (total) | Item                                | Link                                                                                      |
| ----- | ---------- | ------------ | ----------------------------------- | ----------------------------------------------------------------------------------------- |
| 5\*   |      $80.0 |       $400.0 | WD Red SN700 NVMe SSD               | https://www.westerndigital.com/products/internal-drives/wd-red-sn700-nvme-ssd#WDS500G1R0C |
| 5\*   |      $50.0 |       $250.0 | 4GB SoQuartz module                 | https://pine64.com/product/soquartz-4gb-compute-module-w/                                 |
| 5\*   |      $30.0 |       $150.0 | SoQuartz Blade                      | https://pine64.com/product/soquartz-blade/                                                |
| 5\*   |      $16.0 |        $80.0 | 16GB EMMC module                    | https://pine64.com/product/16gb-emmc-module/                                              |
| 5\*   |       $5.5 |        $27.5 | USB to Type H Barrel Power Cable    | https://pine64.com/product/2-meters-length-usb-to-type-h-barrel-power-cable/              |
| ----- | ---------- | ------------ | ----------------------------------- | ----------------------------------------------------------------------------------------- |
| 1     |      $47.0 |        $47.0 | Compute Module 4 IO board           | https://www.raspberrypi.com/products/compute-module-4-io-board/                           |
| 1     |      $30.0 |        $30.0 | Anker 360 Charger (60W)             | https://www.anker.com/products/a2123?variant=37436925477014&ref=collectionBuy             |
| 1     |       $5.0 |         $5.0 | USB Adapter for EMMC Module         | https://pine64.com/product/usb-adapter-for-emmc-module/                                   |
| 1     |       $2.0 |         $2.0 | Serial Console "Woodpecker" Edition | https://pine64.com/product/serial-console-woodpecker-edition/                             |

\*5 of would be nice, but starting with just 1 makes the most sense

#### Base Price
_(1 of each)_
$265.5

#### Max Price
_(5 of each)_
$991.5

## Timeline:
1. February 1, 2023
  - Have Proof-of-Concept SoQuartz Blade(s)
  - NixOS booting reliably
  - k3s installed
  - ceph
  - metallb
  - ingress-nginx
  - cert-manager
2. March 1, 2023
  - Mastodon migrated to blade cluster
  - pict-rs migrated to blade cluster
  - relay migrated to blade cluster
3. April 1, 2023
  - Remaining services migrated to SoQuartz blade cluster
  - Extra Rock64s added into cluster
  - Raspberry Pi 4s retired
